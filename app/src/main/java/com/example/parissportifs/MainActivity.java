package com.example.parissportifs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.parissportifs.data.Bet;
import com.example.parissportifs.data.BetViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recycle_bet);
        recyclerView.setAdapter(new MainActivity.SimpleItemRecyclerViewAdapter(this));

    }

    private class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {
        private final List<Bet> mValues;

        public SimpleItemRecyclerViewAdapter(MainActivity mainActivity) {
            BetViewModel betModel = new ViewModelProvider(mainActivity).get(BetViewModel.class);
            betModel.initAPI();
            mValues = new ArrayList<>(betModel.getBets().getValue());
            betModel.getBets().observe(mainActivity, bets -> {
                mValues.clear();
                mValues.addAll(bets);
                notifyDataSetChanged();
            });
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.home_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            // Récupérer un bet en fonctions de la position
           Bet bet = mValues.get(position);
           //holder.rencontre.setText((bet.get()));

            holder.competition.setText(bet.getName());
            //holder.rencontre.setText(Integer.toString(bet.getBet_id()));
            //holder.competition.setText(bet.getList_odds());
            //holder.itemView.setOnClickListener(mOnClickListener);
            holder.itemView.setTag(mValues.get(position));
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final ImageView icon_sport;
            final TextView rencontre;
            final TextView competition;

            ViewHolder(View view) {
                super(view);
                icon_sport = (ImageView) view.findViewById(R.id.icon_sport);
                rencontre = (TextView) view.findViewById(R.id.text_rencontre);
                competition = (TextView) view.findViewById(R.id.text_competition);
            }
        }
    }


}