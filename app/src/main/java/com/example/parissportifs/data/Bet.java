package com.example.parissportifs.data;


import java.time.format.DateTimeFormatter;

public class Bet {

    private String name;
    private Integer bet_id;
    private String list_odds;
    private String sports;


    // champs custom
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBet_id() {
        return bet_id;
    }

    public void setBet_id(Integer bet_id) {
        this.bet_id = bet_id;
    }

    public String getList_odds() {
        return list_odds;
    }

    public void setList_odds(String list_odds) {
        this.list_odds = list_odds;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }
}
