package com.example.parissportifs.data;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BetApiService
{
    @GET("allBets")
    Call<List<Bet>> listBets();
}
