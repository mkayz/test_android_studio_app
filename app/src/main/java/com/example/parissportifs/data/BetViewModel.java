package com.example.parissportifs.data;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BetViewModel extends ViewModel {

    private MutableLiveData<List<Bet>> bets = new MutableLiveData<>(new ArrayList<>());

    public LiveData<List<Bet>> getBets()
    {
        return bets;
    }

    public void init(){
        List<Bet> list = new LinkedList<>();
        // ici on construit de la même façon mais avec un appel vers l'API
        //list.add(new Bet("psg - OM", "Ligue1 UberEats", "football"));
        //list.add(new Bet("Lille - Rouen", "Ligue1 UberEats", "football"));

        //list.add(new Bet());
        bets.setValue(list);
    }

    public void initAPI() {
        List<Bet> list = new LinkedList<>();

        // retrofit

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BetApiService api = retrofit.create(BetApiService.class);
        Call<List<Bet>> call = api.listBets();

        try {
            call.enqueue(new Callback<List<Bet>>() {
                @Override
                public void onResponse(Call<List<Bet>> call, Response<List<Bet>> response) {
                    List<Bet> users = response.body();

                    bets.setValue(users);

                }

                @Override
                public void onFailure(Call<List<Bet>> call, Throwable t) {
                    int I = 0;
                }

            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        // appeler retrofit ici sur le bet


    }
}
